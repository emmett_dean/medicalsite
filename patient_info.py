class Patient:
  def __init__(self, name ,dob , symptoms , preexisting_conditions, insurance, ssn):
    self.name = name
    self.dob = dob
    self.symptoms = symptoms
    self.preexisting_conditions = preexisting_conditions
    self.insurance = insurance
    self.ssn = ssn
  def get(self):
    return (self.name, self.dob, self.symptoms, self.preexisting_conditions, self.insurance, self.ssn)
  def __str__(self):
    return f" {self.name}  {self.dob}  {self.symptoms} {self.preexisting_conditions}  {self.insurance}  {self.ssn}"
class Conditions:
  def __init__(self, preexisting_conditions, severity):
    self.preexisting_conditions = preexisting_conditions
    self.severity = severity
  def get(self):
    return (self.preexisting_conditions, self.severity)
  def __str__(self):
    return f" {self.preexisting_conditions} {self.severity}"