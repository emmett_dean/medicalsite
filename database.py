import sqlite3
class Database:
  def create_info (c):
    c.execute("CREATE TABLE IF NOT EXISTS patient_info (name varchar(30), dob varchar(10), symptoms varchar(100), preexisting_conditions varchar(100), insurance varchar(11), ssn varchar(12), unique(name, dob, symptoms, preexisting_conditions, insurance, ssn))")
  
  def insert_info (c, name, dob, symptoms, preexisting_conditions, insurance, ssn):
    try:
      c.execute("insert into patient_info values(?, ?, ?, ?, ?, ?)", (name, dob, symptoms, preexisting_conditions, insurance,  ssn))
    except sqlite3.IntegrityError:
      print("Insertion Fail") 
  def select_info (c):
    c.execute("select * from patient_info")
  def create_conditions (c):
    c.execute("CREATE TABLE IF NOT EXISTS conditions (preexisting_conditions varchar(300), severity integer(10))")
  def insert_conditions (c, preexisting_conditions, severity):
    try:
      c.execute("insert into conditions values(?,?)", (preexisting_conditions, severity))
    except sqlite3.IntegrityError:
      print("Insertion Fail") 
  def select_conditions (c):
    c.execute("select * from patient_info")